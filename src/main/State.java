package main;

public interface State {
    String write();
}
