package test;

import main.BoldText;
import main.Italics;
import main.TextRedactor;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TestRedactor {
    @Test
    public void test() {
        TextRedactor textRedactor = new TextRedactor();
        textRedactor.setState(new Italics());
        assertEquals("Пишем курсивом", textRedactor.getState().write());
    }

    @Test
    public void test1() {
        TextRedactor textRedactor = new TextRedactor();
        textRedactor.setState(new BoldText());
        assertEquals("Пишем жирным текстом", textRedactor.getState().write());
    }
}
